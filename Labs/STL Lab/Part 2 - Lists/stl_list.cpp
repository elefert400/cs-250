// Lab - Standard Template Library - Part 2 - Lists
// FIRSTNAME, LASTNAME

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list<string>& states);


int main()
{

	bool done = false;

		list<string> TheList;

		while (!done)
		{
			cout << "MAIN MENU " << endl;
			cout << "1. Add new states to front of list "
				<< "2. Add new state to back of list "
				<< "3. Pop state from front of list "
				<< "4. Pop state from end of list "
				<< "5. Continue" << endl;

			int choice;
			cin >> choice;

			if (choice == 1)
			{
				string state;
				cout << "Add new state to front " << endl;
					cin >> state;
				TheList.push_front(state);
					
			}

			else if (choice == 2)
			{
				string stateB;
					cout << "Add a new state to the back " << endl;
					cin >> stateB;
				TheList.push_back(stateB);
			}

			else if (choice == 3)
			{
				TheList.pop_front();
			}

			else if (choice == 4)
			{
				TheList.pop_back();
			}

			else if (choice == 5)
			{
				done = true;
			}
			
			cout << "Normal List: " << endl;
			DisplayList(TheList);

			cout << "Reverse List: " << endl;
			TheList.reverse();
			DisplayList(TheList);

			cout << "Sort List: " << endl;
			TheList.sort();
			DisplayList(TheList);

			cout << "Reverse Sorted List: " << endl;
			TheList.reverse();
			DisplayList(TheList);
			


			
		}

    cin.ignore();
    cin.get();
    return 0;
}



void DisplayList(list<string>& states)
{
	for (list<string> ::iterator it = states.begin(); it != states.end(); it++)
	{
		cout << *it << "\t";
	}
	cout << endl;
}


