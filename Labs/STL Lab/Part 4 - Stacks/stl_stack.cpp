// Lab - Standard Template Library - Part 4 - Stacks
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> letters;
	bool done = false;

	while (!done)
	{
		cout << "Main Menu: " << endl;
		cout << "1. Enter next letter"
			<< "2. Undo last letter"
			<< "3. Done" << endl;
		int choice;
			cin >> choice;

			if (choice == 1)
			{
				cout << "Letter: " << endl;
				string letter;
				cin letter;
				letters.pop(letter);
			}
			else if (choice == 2)
			{
				letters.pop();
			}
			else if (choice == 3)
			{
				done = true;
			}
	}

	while (!letters.empty())
	{
		cout << letters.top() << endl;
		letters.pop();
	}

    cin.ignore();
    cin.get();
    return 0;
}
