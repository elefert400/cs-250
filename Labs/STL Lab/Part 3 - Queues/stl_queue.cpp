// Lab - Standard Template Library - Part 3 - Queues
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> transactions;

	bool done = false;

		while (!done)
		{
			cout << "MAIN MENU " << endl;
			cout << "1. Add new deposit "
				<< "2. Add new withdraw "
				<< "3. Continue " << endl;
			int choice;
				cin >> choice;

				if (choice == 1)
				{
					int deposit;
						cin >> deposit;
						transactions.push(deposit);
				}
				else if (choice == 2)
				{
					int withdraw;
					cin >> withdraw;
					transactions.push(withdraw);
				}
				else if (choice == 3)
				{
					done = true;
				}
		}

			float total = 0;

			while (!transactions.empty())
			{
				total += transactions.front();
					transactions.pop();
			}

			cout << "total: " << total << endl;



    cin.ignore();
    cin.get();
    return 0;
}
