#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"

#include "LinkedList.hpp"

using namespace std;

template <typename T>
class LinkedStack : public LinkedList<T>
 {
    public:
    LinkedStack()
    {
		m_ptrFirst = nullptr;
		m_ptrLast = nullptr;
		m_itemCount = 0;
    }

    void Push( const T& newData )
	{
		LinkedList<T>::PushBack(newData);
		m_itemCount++;
    }

    T& Top()
    {
			return LinkedList<T>::GetBack();
    }

    void Pop()
    {
		LinkedList<T>::PopBack(); 
		m_itemCount--;
    }

    int Size()
    {
		return m_itemCount;
    }

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
