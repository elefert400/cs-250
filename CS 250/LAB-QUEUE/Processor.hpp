#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "First Come, First Served" << endl;
	int cycleCounter = 0;
	int completionTime = 0;
	while (jobQueue.Size() > 0)
	{
		output << "Cycle: " << cycleCounter << endl;
		output << "Job ID: " << jobQueue.Front()->id << endl;
		output << "Time Remaining" << jobQueue.Front()->fcfs_timeRemaining << endl;
		jobQueue.Front()->Work(FCFS);
		if (jobQueue.Front()->fcfs_done)
		{
			jobQueue.Front()->fcfs_finishTime = cycleCounter;
			jobQueue.Pop();
		}
		cycleCounter++;
	}
	for (int i = 0; i < allJobs.size(); i++)
	{
		completionTime = completionTime + allJobs[i].fcfs_finishTime;
		output << "Job ID: " << allJobs[i].id << endl;
		output << "Time to Completion: " << allJobs[i].fcfs_finishTime << endl;
	}
	double average = completionTime / allJobs.size();
	output << "The Total Completion Time is: " << completionTime << endl;
	output << "The Average Job Finish Time is: " << average << endl;
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Round Robin" << endl;
	int cycles = 0;
	int timer = 0;
	int completionTime = 0;
	while (jobQueue.Size() > 0)
	{
		output << "Cycle: " << cycles << endl;
		output << "Job ID: " << jobQueue.Front()->id << endl;
		output << "Time Remaining" << jobQueue.Front()->rr_timeRemaining << endl;
		output << "Times Interrupted" << jobQueue.Front()->rr_timesInterrupted << endl;
		if (timer == timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;
		}

		jobQueue.Front()->Work(RR);
		if (jobQueue.Front()->rr_done)
		{
			timer = timer + (timePerProcess * jobQueue.Front()->rr_timesInterrupted);
			jobQueue.Front()->SetFinishTime(timer, RR);
			jobQueue.Pop();
		}
		cycles++;
		timer++;
	}
	for (int i = 0; i < allJobs.size(); i++)
	{
		completionTime = completionTime + allJobs[i].rr_finishTime;
		output << "Job ID: " << allJobs[i].id << endl;
		output << "Time to Completion: " << allJobs[i].rr_finishTime << endl;
		output << "Times Interrupted: " << allJobs[i].rr_timesInterrupted << endl;
	}
	double average = completionTime / allJobs.size();
	output << "The Total Completion Time is: " << completionTime << endl;
	output << "The Average Job Finish Time is: " << average << endl;
	output << "Red Robin Interval: "<< cycles << endl;
}

#endif
