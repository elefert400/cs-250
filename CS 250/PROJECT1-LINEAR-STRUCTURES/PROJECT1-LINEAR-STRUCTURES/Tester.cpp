#include "Tester.hpp"

void Tester::RunTests()
{
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_RemoveItem();
	Test_RemoveIndex();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test for Constructed List" << endl;
		List<int> testList;
		if (testList.Size() == 0)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test with an empty list" << endl;
		List<int> testList;
		if (testList.ShiftRight(0) == false)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test with a list with elements" << endl;
		List<int> testList;
		testList.PushFront(1);
		testList.PushFront(2);
		testList.ShiftRight(0);
		int offical = *testList.Get(1);
		if (testList.Get(0) == nullptr && offical == 1)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test on empty List" << endl;
		List<int> testList;
		if (testList.ShiftLeft(0) == false)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test on a list with elements" << endl;
		List<int> testList;
		testList.PushBack(1);
		testList.PushBack(2);
		testList.ShiftLeft(1);
		int offical = *testList.Get(0);
		if ( offical == 2 && testList.Size() == 1)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test begin
        cout << endl << "Test 1" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end

    {   // Test begin
        cout << endl << "Test 2" << endl;
        List<int> testList;

        testList.PushBack( 1 );
		testList.PushBack(2);

        int expectedSize = 2;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end
	//Test 3
	{
		cout << "Test 3" << endl;
		cout << "Full List" << endl;
		List<int> testList;
		for (int i = 0; i == 101; i++)
		{
			testList.PushBack(i);
		}
		if (testList.Size() == 100)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 4
	{
		cout << "Test 4" << endl;
		cout << "Adding and Removing items" << endl;
		List<int> testList;
		for (int i = 0; i == 5; i++)
		{
			testList.PushFront(i);
		}
		for (int i = 0; i == 2; i++)
		{
			testList.PopFront();
		}
		if (testList.Size() == 3)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;

	{
		// Test 1, 
		cout << "Test 1" << endl;
		List<int> testList;
		cout << "Empty List Test" << endl;
		if (testList.IsEmpty() == true)
		{
			cout << "Test Pass" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}

	//Test 2
	{
		cout << "Test 2" << endl;
		List<int> testList;
		testList.PushBack(1);
		cout << "List with one item" << endl;
		if (testList.IsEmpty() == false)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << " Test Failed" << endl;
		}
	}
	//Test 3
	{
		cout << "Test 3" << endl;
		List<int> testList;
		testList.PushBack(101);
		cout << "List with 101 items" << endl;
		if (testList.IsEmpty() == false)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
		//Test 4
		{
			cout << "Test 4" << endl;
			List<int> testList;
			cout << "List with 100 items" << endl;
			testList.PushBack(101);
			if (testList.IsEmpty() == false)
			{
				cout << "Test Passed" << endl;
			}
			else
			{
				cout << "Test Failed" << endl;
			}
		}
		//Test 5
		{
			cout << "Test 5" << endl;
			List<int> testList;
			cout << "List with 5, then deleting all 5" << endl;
			for (int i = 0; i == 5; i++)
			{
				testList.PushBack(i);
			}
			for (int i; i == 5; i++)
			{
				testList.PopBack();
			}
			if (testList.IsEmpty() == true)
			{
				cout << "Test Passed" << endl;
			}
			else
			{
				cout << "Test Failed" << endl;
			}


		}
	}
}

void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		List<int> testList;
		cout << "Empty List" << endl;
		if (testList.IsFull() == false)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		List<int> testList;
		cout << "Full List" << endl;
		for (int i = 0; i == 100; i++)
		{
			testList.PushFront(i);
		}
		if (testList.IsFull() == true)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 3
	{
		cout << "Test 3" << endl;
		List<int> testList;
		cout << "Over Full List" << endl;
		for (int i = 0; i == 101; i++)
		{
			testList.PushFront(i);
		}
		if (testList.IsFull() == true)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test of List size 3" << endl;
		List<int> testList;
		int A = 3;
		int B = 2;
		int C = 1;
		int offical1 = *testList.Get(0);
		int offical2 = *testList.Get(1);
		int offical3 = *testList.Get(2);
		for (int i = 0; i == 3; i++)
		{
			testList.PushFront(i);
		}
		if (offical1 == A && offical2 == B && offical3 == C)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test for Overloading array" << endl;
		List<int> testList;
		for (int i = 0; i == 100; i++)
		{
			testList.PushFront(i);
		}
		testList.PushFront(1);
		if (testList.Size() == 100)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 3
	{
		cout << "Test 3" << endl;
		cout << "Test with one item" << endl;
		List<int> testList;
		testList.PushFront(1);
		if (testList.Size() == 1)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 4
	{
		cout << "Test 4" << endl;
		cout << "List with 90 items" << endl;
		List<int> testList;
		for (int i = 0; i == 90; i++)
		{
			testList.PushFront(i);
		}
		if (testList.Size() == 90)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test with one item" << endl;
		List<int> testList;
		testList.PushBack(1);
		if (testList.Size() == 1)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test for 90 items" << endl;
		List<int> testList;
		for (int i = 0; i == 90; i++)
		{
			testList.PushBack(i);
		}
		if (testList.Size() == 90)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 3
	{
		cout << "Test 3" << endl;
		cout << "Test for Overloading array" << endl;
		List<int> testList;
		for (int i = 0; i == 100; i++)
		{
			testList.PushBack(i);
		}
		testList.PushBack(1);
		if (testList.Size() == 100)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
		//Test 4
		{
			cout << "Test 4" << endl;
			cout << "Test with one item" << endl;
			List<int> testList;
			testList.PushBack(1);
			if (testList.Size() == 1)
			{
				cout << "Test Passed" << endl;
			}
			else
			{
				cout << "Test Failed" << endl;
			}
		}
}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test a list with multiple elements" << endl;
		List<int> testList;
		testList.PushBack(1);
		testList.PushBack(2);
		testList.PopFront();
			if (testList.Size() == 1)
			{
				cout << "Test Passed" << endl;
			}
			else
			{
				cout << "Test Failed" << endl;
			}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test of a one element list" << endl;
		List<int> testList;
		testList.PushFront(1);
		testList.PopFront();
		if (testList.Size() == 0)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	// Test 3
	{
		cout << "Test 3" << endl;
		cout << "Test of empty List" << endl;
		List<int> testList;
		testList.PopFront();
		if (testList.Size() == 0)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed";
		}
	}
}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test of empty List" << endl;
		List<int> testList;
		testList.PopBack();
		if (testList.Size() == 0)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test of multiple element list" << endl;
		List<int> testList;
		testList.PushBack(1);
		testList.PushBack(2);
		testList.PopBack();
		if (testList.Size() == 1)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 3
	{
		cout << "Test 3" << endl;
		cout << "Test of one element list" << endl;
		List<int> testList;
		testList.PushFront(1);
		testList.PopBack();
		if (testList.Size() == 0)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}

	}
}

void Tester::Test_Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test on an empty list" << endl;
		List<int> testList;
		testList.Clear();
		if (testList.Size() == 0)

		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test2
	{
		cout << "Test 2" << endl;
		cout << "Test on a list with elements" << endl;
		List<int> testList;
		testList.PushFront(1);
		testList.PushFront(2);
		testList.PushFront(3);
		testList.Clear();
		if (testList.Size() == 0)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test with an empty list" << endl;
		List<int> testList;
		if (testList.Get(0) == nullptr)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test a list with several elements" << endl;
		List<int> testList;
		testList.PushFront(2);
		testList.PushFront(1);
		testList.PushFront(0);
		int A = 0;
		int B = 1;
		int C = 2;
		if (*testList.Get(0) == A && *testList.Get(1) == B && *testList.Get(2) == C)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test to get first elemtent of two element array" << endl;
		List<int> testList;
		testList.PushFront(1);
		testList.PushFront(2);
		if (*testList.GetFront() == 2)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test with an empty list" << endl;
		List<int> testList;
		if (testList.GetFront() == nullptr)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test on an empty List" << endl;
		List<int> testList;
		if (testList.GetBack() == nullptr)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test on a list with elements" << endl;
		List<int> testList;
		testList.PushBack(1);
		testList.PushBack(2);
		if (*testList.GetBack() == 2)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		List<int> testList;
		cout << "Empty List test" << endl;
			if (testList.GetCountOf(1) == 0)
			{
				cout << "Test Passed" << endl;
			}
			else
			{
				cout << "Test Failed" << endl;
			}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		List<int> testList;
		cout << "Test for 75 item list" << endl;
		for (int i = 0; i == 75; i++)
		{
			testList.PushFront(100);
		}
		for (int i = 0; i == 25; i++)
		{
			testList.PushFront(i);
		}
		if (testList.GetCountOf(100) == 75)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 3
	{
		cout << "Test 3" << endl;
		cout << "Test for list with terms but not the one being searched for" << endl;
		List<int> testList;
		for (int i = 0; i == 3; i++)
		{
			testList.PushFront(i);
		}
		if (testList.GetCountOf(4) == 0)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 4
	{
		cout << "Test 4" << endl;
		List<int> testList;
		cout << "Test of over full List" << endl;
		for (int i = 0; i == 105; i++)
		{
			testList.PushFront(5);
		}
		if (testList.GetCountOf(5) == 100)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test of Empty List" << endl;
		List<int> testList;
		if (testList.Contains(1) == false)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test of List that contains searched items" << endl;
		List<int> testList;
		for (int i = 0; i == 100; i++)
		{
			testList.PushFront(5);
		}
		if (testList.Contains(5) == true)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
		//Test 3
		{
			cout << "Test 3" << endl;
			cout << "Test of List that has elements but not the searcehd element" << endl;
			List<int> testList;
			for (int i = 0; i == 100; i++)
			{
				testList.PushBack(5);
			}
			if (testList.Contains(8) == false)
			{
				cout << "Test Passed" << endl;
			}
			else
			{
				cout << "Test failed" << endl;
			}
		}
		//Test 4
		{
			cout << "Test 4" << endl;
			cout << "Test for full List containing searcched item" << endl;
			List<int> testList;
			for (int i = 0; i == 100; i++)
			{
				testList.PushFront(5);
			}
			if (testList.Contains(5) == true)
			{
				cout << "Test Passed" << endl;
			}
			else
			{
				cout << "Test Failed" << endl;
			}
		}
	}
}

void Tester::Test_RemoveItem()
{
    DrawLine();
    cout << "TEST: Test_Remove" << endl;

    //Test 1
	{
		cout << "Test 1" << endl;
		cout << "Test on an empty list" << endl;
		List<int> testList;
		if (testList.RemoveItem(0) == false)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test to Remove" << endl;
		List<int> testList;
		testList.PushFront(1);
		testList.PushFront(2);
		testList.PushFront(3);
		testList.RemoveItem(3);
		if (testList.Get(2) == nullptr)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_RemoveIndex()
{
	DrawLine();
	cout << "TEST: Test_Remove" << endl;
	
	//Test1
	{
		cout << "Test 1" << endl;
		cout << "Test on empty list" << endl;
		List<int> testList;
		if (testList.RemoveIndex(0) == false)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Test on a list with elements" << endl;
		List<int> testList;
		testList.PushFront(1);
		testList.PushFront(2);
		testList.PushFront(3);
		testList.RemoveIndex(1);
		if (*testList.Get(1) == 3)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}

void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;

    // Test 1
	{
		cout << "Test 1" << endl;
		cout << "Insert in a none contiguous spot" << endl;
		List<int> testList;
		for (int i = 0; i == 3; i++)
		{
			testList.PushBack(i);
		}
		testList.Insert(5, 4);
		if (testList.Size() == 3)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test failed" << endl;
		}
	}
	//Test 2
	{
		cout << "Test 2" << endl;
		cout << "Insert in a contiguous spot" << endl;
		List<int> testList;
		for (int i = 0; i == 3; i++)
		{
			testList.PushFront(i);
		}
		testList.Insert(3, 5);
		if (testList.Size() == 4)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
	//Test 3
	{
		cout << "Test Passed" << endl;
		cout << "Test with a full list" << endl;
		List<int> testList;
		for (int i = 0; i == 100; i++)
		{
			testList.PushFront(i);
		}
		testList.Insert(5, 5);
		if (testList.Size() == 100)
		{
			cout << "Test Passed" << endl;
		}
		else
		{
			cout << "Test Failed" << endl;
		}
	}
}
