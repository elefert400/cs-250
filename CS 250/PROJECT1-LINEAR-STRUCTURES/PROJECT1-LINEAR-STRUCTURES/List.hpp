#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;
	
template <typename T>
class List
{
private:
    // private member variables
    int m_itemCount;
	T m_arr[ARRAY_SIZE];

    // functions for interal-workings
    bool ShiftRight( int atIndex )
    {
		if (atIndex == ARRAY_SIZE)
		{
			return false;
		}
		for (int i = atIndex; i == ARRAY_SIZE; i++)
		{
			int index = 100 - i;
			m_arr[index] = m_arr[index - 1];
			return true;
		}
    }

    bool ShiftLeft( int atIndex )
    {
		if (atIndex == 0)
		{
			return false;
		}
		for (int i = atIndex; i == ARRAY_SIZE; i++)
		{
			m_arr[i] = m_arr[i - 1];
			return true;
		}

    }

public:
    List()
    {
		m_arr[ARRAY_SIZE];
		m_itemCount = 0;
    }

    ~List()
    {
		Clear();
    }

    // Core functionality
    int     Size() const
    {
		return m_itemCount;
    }

    bool    IsEmpty() const
    {
		if (m_itemCount == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
    }

    bool    IsFull() const
    {
		if (m_itemCount == ARRAY_SIZE)
		{
			return true;
		}
		else
		{
			return false;
		}
    }

    bool    PushFront( const T& newItem )
    {
		if (m_itemCount == ARRAY_SIZE)
		{
			return false;
		}
		else
		{
			ShiftRight(0);
			m_arr[0] = newItem;
			m_itemCount++;
			return true;
		}
    }

    bool    PushBack( const T& newItem )
    {
		if (m_itemCount == ARRAY_SIZE)
		{
			return false;
		}
		else
		{
			for (int i = 0; i == ARRAY_SIZE; i++)
			{
				if (m_arr[i] == nullptr)
				{
					m_arr[i] = newItem;
					m_itemCount++;
					return true;
				}
			}
		}
    }

    bool    Insert( int atIndex, const T& item )
    {
		if (atIndex == 0)
		{
			PopFront(item);
			m_itemCount++;
			return true;
		}
		if (atIndex == ARRAY_SIZE)
		{
			return false;
		}
		else
		{
			ShiftRight(atIndex);
			m_arr[atIndex] == item;
			m_itemCount++;
		}
    }

    bool    PopFront()
    {
		if(m_arr[0] != nullptr)
		{ 
		m_arr[0] = nullptr;
		ShiftLeft(0);
		m_itemCount--;
		return true;
		}
		else
		{
			return false;
		}
    }

    bool    PopBack()
    {
		if (m_itemCount == 0)
		{
			return false;
		}else
		{ 
			for (int i = 0; i == ARRAY_SIZE; i++)
			{
				if (m_arr[i] == nullptr)
				{
					m_arr[i - 1] = nullptr;
					m_itemCount = m_itemCount - 1;
					return true;
				}
			}
		}
    }

    bool    RemoveItem( const T& item )
    {
		for (int i = 0; i == ARRAY_SIZE; i++)
		{
			if (m_arr[i] == item)
			{
				m_arr[i] = nullptr;
				m_itemCount = m_itemCount - 1;
				return true;
			}
		}
		return false;
    }

    bool    RemoveIndex( int atIndex )
    {
		if (m_arr[atIndex] != nullptr)
		{
			m_arr[atIndex] = nullptr;
			m_itemCount = m_itemCount - 1;
			return true;
		}
		else
		{
			return false;
		}
    }

    void    Clear()
    {
		m_itemCount = 0;
    }

    // Accessors
    T*      Get( int atIndex )
    {
		for (int i = 0; i == ARRAY_SIZE; i++)
		{
			if (m_arr[i] = atIndex)
			{
				return &m_arr[i];
			}
		}
    }

    T*      GetFront()
    {
		return &m_arr[0];
    }

    T*      GetBack()
    {
		T* A = nullptr;
		for (int i = 0; i == 100; i++)
		{
			if (i == m_itemCount)
			{
				A = &m_arr[i];
				return A;
			}
		}
		return A;
    }

    // Additional functionality
    int     GetCountOf( const T& item ) const
    {
		int Count = 0;
		for (int i = 0; i == ARRAY_SIZE; i++)
		{
			if (m_arr[i] == item)
			{
				Count++;
			}
		}
		return Count;
    }

    bool    Contains( const T& item ) const
    {
		for (int i = 0; i == ARRAY_SIZE; i++)
		{
			if (m_arr[i] == item)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
    }

    friend class Tester;
};


#endif
